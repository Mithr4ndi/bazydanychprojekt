use s13883;
SET DATEFORMAT dmy;
GO

/* Trigger nie pozwoli wstawic mapy, na ktorej liczba graczy jest <= 1 lub > 40 lub jest nieparzysta*/
ALTER
--CREATE 
TRIGGER  insertMap ON Map
FOR INSERT
AS
DECLARE 
@Id_Map INT,
@NrP INT,
@NrP_old INT,
@MapName VARCHAR(40);
DECLARE curMap CURSOR FOR SELECT Id_Map, Map_Name, Number_of_players FROM INSERTED;
BEGIN
	OPEN curMap;
	FETCH NEXT FROM curMap INTO @Id_Map, @MapName, @NrP;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @NrP <= 1 OR @NrP > 40 OR (@NrP % 2 != 0)
		BEGIN
		DELETE FROM Map WHERE Id_Map = @Id_Map;
		PRINT 'Mapa : ' + @MapName + ' nie zostala wstawiona!'
		END
		ELSE
		PRINT 'Mapa : ' + @MapName + ' zostala wstawiona!'
	FETCH NEXT FROM curMap INTO @Id_Map, @MapName, @NrP;
	END
	CLOSE curMap; 
	DEALLOCATE curMap; 	
END

GO
 -------insertMap---------------------------
 INSERT INTO Map(Map_Name, Number_of_players) VALUES ('Dark', 1),('Water', 1), ('Fire', 10);
 SELECT * FROM Map;
 ------------------------------------------
 GO


 /* Trigger sprawdza czy dodawany gracz ma ukonczone 18lat, jesli nie zglasza blad. */

 ALTER
--CREATE 
TRIGGER  insertPlayer ON Player
FOR INSERT
AS
DECLARE
@Id_Player INT,
@Name VARCHAR(20),
@Birth_Date DATE;
DECLARE curPlayer CURSOR FOR SELECT Id_Player, Last_Name, Birth_Date FROM INSERTED;
BEGIN
OPEN curPlayer;
	FETCH NEXT FROM curPlayer INTO @Id_Player, @Name, @Birth_Date;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF DATEDIFF(yy, @Birth_Date, GETDATE()) < 18
			BEGIN
			RAISERROR('Gracz: ' +@Name + ' nie ma ukonczonych 18 lat!' , 1 , 10)
			DELETE FROM Player WHERE Id_Player = @Id_Player;
			END
		ELSE
			PRINT 'GRACZ: ' + @Name + ' ZOSTAL DODANY';
	FETCH NEXT FROM curPlayer INTO @Id_Player, @Name, @Birth_Date;	
	END
	CLOSE curPlayer; 
	DEALLOCATE curPlayer; 				
END;

GO
-------insertPlayer---------------------------
INSERT INTO Player VALUES ('William', 'Turner', 'ThePirate', 'haselo456', 4, '05/07/08', 'will.T@disney.com', '13/06/17', '13/06/17');
INSERT INTO Player VALUES ('Elisabeth', 'Swan', 'Swan123', 'haselo45656', 4, '05/07/06', 'will.T@disney.com', '13/06/17', '13/06/17');
INSERT INTO Player VALUES ('Jack', 'Sparrow', 'BarbossaNoob', 'haselo4123', 1, '05/07/99', 'will.T@disney.com', '13/06/17', '13/06/17');

---------------------------------------------
GO

 /*Trigger nie pozwoli zaatkualizowac mapy, na ktorej liczba graczy jest <= 1 lub > 40 lub jest nieparzysta, 
 wypisze stosowny komunikat i wstawi wartosc domyslna rowna 2*/

ALTER
--CREATE 
TRIGGER  updateMap ON Map
FOR UPDATE
AS
DECLARE 
@Id_Map INT,
@NrP INT,
@MapName VARCHAR(40);;
DECLARE curMap CURSOR FOR SELECT Id_Map,Map_Name, Number_of_players FROM Inserted;
BEGIN
	OPEN curMap;
	FETCH NEXT FROM curMap INTO @Id_Map, @MapName, @NrP
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @NrP <= 1 OR @NrP > 40 OR (@NrP % 2 != 0)
			BEGIN
			UPDATE Map SET Number_of_players = 2 WHERE Id_Map = @Id_Map;
			RAISERROR( 'Warosc Number_of_players  musi miescic sie w zakresie od 2 do 40 i byc parzysta!', 1, 10);
			PRINT 'WSTAWIONO DOMYSLNA WARTOSC ROWNA 2! DO MAPY: ' + @MapName;
			END
		ELSE 
			PRINT 'LICZBA GRACZY ZOSTALA ZAATKUALIZOWANA! MAPA: ' + @MapName;

	FETCH NEXT FROM curMap INTO @Id_Map,@MapName, @NrP;
	END
	CLOSE curMap; 
	DEALLOCATE curMap; 
END;

GO
 -------insertMap---------------------------
UPDATE Map SET Number_of_players = 1 WHERE Map_Name = 'Eye of Eternity';
UPDATE Map SET Number_of_players = 41 WHERE Map_Name = 'Warsong Gulch';
UPDATE Map SET Number_of_players = 3 WHERE Map_Name = 'Isle od Conquest';
UPDATE Map SET Number_of_players = 4 WHERE Map_Name = 'Dalaran Arena';
SELECT * FROM Map;
 ------------------------------------------

