use s13883;
SET DATEFORMAT dmy;

CREATE TABLE Nationality (
                Id_Nation INT IDENTITY NOT NULL,
                Nationality VARCHAR(40) NOT NULL,
                CONSTRAINT Nationality_pk PRIMARY KEY (Id_Nation)
)

CREATE TABLE Race (
                Id_Race INT IDENTITY NOT NULL,
                R_Name VARCHAR(40) NOT NULL,
                CONSTRAINT Race_pk PRIMARY KEY (Id_Race)
)

CREATE TABLE Team (
                Id_Team INT IDENTITY NOT NULL,
                Team_Name VARCHAR(40) NOT NULL,
                CONSTRAINT Team_pk PRIMARY KEY (Id_Team)
)

CREATE TABLE Spell_Kind (
                Id_SK INT NOT NULL,
                Full_Name VARCHAR(40) NOT NULL,
                Short_Name VARCHAR(40) NOT NULL,
                CONSTRAINT Spell_Kind_pk PRIMARY KEY (Id_SK)
)


CREATE TABLE SM_SK (
                Id_SM_SK INT IDENTITY NOT NULL,
                Id_SK1 INT NOT NULL,
                Id_SK2 INT NOT NULL,
                Id_SK3 INT NOT NULL,
                CONSTRAINT SM_SK_pk PRIMARY KEY (Id_SM_SK)
)


CREATE TABLE Class (
                Id_Class INT IDENTITY NOT NULL,
                Class_Name VARCHAR(40) NOT NULL,
                CONSTRAINT Class_pk PRIMARY KEY (Id_Class)
)

CREATE TABLE Event_Type (
                Id_ET INT IDENTITY NOT NULL,
                ET_Name VARCHAR(40) NOT NULL,
                Description VARCHAR(40) NOT NULL,
                CONSTRAINT Event_Types_pk PRIMARY KEY (Id_ET)
)

CREATE TABLE Event (
                Id_Event INT IDENTITY NOT NULL,
                Id_ET INT NOT NULL,
                Event_Name VARCHAR(40) NOT NULL,
                Stop_Date DATE NOT NULL,
                Start_Date DATE NOT NULL,
                CONSTRAINT Event_pk PRIMARY KEY (Id_Event)
)

CREATE TABLE Game_Mode (
                Id_GM INT IDENTITY NOT NULL,
                GM_Name VARCHAR(40) NOT NULL,
                Description VARCHAR(40) NOT NULL,
                CONSTRAINT Game_Mode_pk PRIMARY KEY (Id_GM)
)

CREATE TABLE Specialization (
                Id_Spec INT IDENTITY NOT NULL,
                Spec_Name VARCHAR(40) NOT NULL,
                CONSTRAINT Specialization_pk PRIMARY KEY (Id_Spec)
)

CREATE TABLE Spec_SM (
                Id_SpecSM INT IDENTITY NOT NULL,
                Id_SM_SK INT NOT NULL,
                Id_Spec INT NOT NULL,
                CONSTRAINT Spec_SM_pk PRIMARY KEY (Id_SpecSM)
)

CREATE TABLE Class_Spec_Rac (
                Id_CSR INT IDENTITY NOT NULL,
                Id_Class INT NOT NULL,
                Id_SpecSM INT,
                Id_Race INT NOT NULL,
                CONSTRAINT Class_Spec_Rac_pk PRIMARY KEY (Id_CSR)
)

CREATE TABLE Weapon (
                Id_Weapon INT IDENTITY NOT NULL,
                Weapon_Name VARCHAR(40) NOT NULL,
                CONSTRAINT Weapon_pk PRIMARY KEY (Id_Weapon)
)

CREATE TABLE Weapon_Class (
                Id_WeaponClass INT IDENTITY NOT NULL,
                Id_Class INT NOT NULL,
                Id_Weapon INT NOT NULL,
                CONSTRAINT Weapon_Class_pk PRIMARY KEY (Id_WeaponClass)
)

CREATE TABLE Staff (
                Id_Weapon INT NOT NULL,
                Damage INT NOT NULL,
                Intellect INT NOT NULL,
                Spirit INT NOT NULL,
                Spell_Power INT NOT NULL,
                CONSTRAINT Staff_pk PRIMARY KEY (Id_Weapon)
)

CREATE TABLE Sword (
                Id_Weapon INT NOT NULL,
                Id_SK INT NOT NULL,
                Damage INT NOT NULL,
                Strange INT NOT NULL,
                Attack_Power INT NOT NULL,
                Haste_Rating INT NOT NULL,
                CONSTRAINT Sword_pk PRIMARY KEY (Id_Weapon)
)


CREATE TABLE Sword_Kind (
                Id_SK INT IDENTITY NOT NULL,
                Kind VARCHAR(40) NOT NULL,
                CONSTRAINT Sword_Kind_pk PRIMARY KEY (Id_SK)
)

CREATE TABLE Shield (
                Id_Weapon INT NOT NULL,
                Stamina INT NOT NULL,
                Strange INT NOT NULL,
                Defense_Rating INT NOT NULL,
                Parry_Rating INT NOT NULL,
                CONSTRAINT Shield_pk PRIMARY KEY (Id_Weapon)
)

CREATE TABLE Bow (
                Id_Weapon INT NOT NULL,
                Damage INT NOT NULL,
                Agility INT NOT NULL,
                Attack_Power INT NOT NULL,
                Haste_Rating INT NOT NULL,
                CONSTRAINT Bow_pk PRIMARY KEY (Id_Weapon)
)

CREATE TABLE Player (
                Id_Player INT IDENTITY NOT NULL,
                Name VARCHAR(40) NOT NULL,
                Last_Name VARCHAR(40) NOT NULL,
                Account_Name VARCHAR(40) UNIQUE NOT NULL,
                Password VARCHAR(40) NOT NULL,
                Id_Nation INT NOT NULL,
                Birth_Date DATE NOT NULL,
                Mail VARCHAR(40) NOT NULL,
                Register_Date DATE,
                Last_online DATE,
                CONSTRAINT Player_pk PRIMARY KEY (Id_Player)
)

CREATE TABLE Character_Build (
                Id_Character INT IDENTITY NOT NULL,
                Id_CSR INT NOT NULL,
                Id_Player INT NOT NULL,
                Char_Name VARCHAR(40) NOT NULL,
                CONSTRAINT Character_Build__pk PRIMARY KEY (Id_Character)
)

CREATE TABLE Player_Team (
                Id_PT INT IDENTITY NOT NULL,
                Id_Team INT NOT NULL,
                Id_Player INT NOT NULL,
                CONSTRAINT Player_Team_pk PRIMARY KEY (Id_PT))

CREATE TABLE Map (
                Id_Map INT IDENTITY NOT NULL,
                Map_Name VARCHAR(40) NOT NULL,
                Number_of_players VARCHAR(40) NOT NULL,
                CONSTRAINT Maps_pk PRIMARY KEY (Id_Map)
)

CREATE TABLE Game (
                Id_Game INT IDENTITY NOT NULL,
                Id_GM INT NOT NULL,
                Id_Map INT NOT NULL,
                Id_Event INT NOT NULL,
                CONSTRAINT Game_pk PRIMARY KEY (Id_Game)
)

CREATE TABLE Game_Team (
                Id_GameTeam INT IDENTITY NOT NULL,
                Id_PT INT NOT NULL,
                Id_Game INT NOT NULL,
                CONSTRAINT Game_Team_pk PRIMARY KEY (Id_GameTeam)
)


CREATE UNIQUE INDEX UQ_Spec_SM ON Spec_SM(Id_SM_SK, Id_Spec);
CREATE UNIQUE INDEX UQ_Class_Spec_Rac ON Class_Spec_Rac(Id_Class, Id_SpecSM, Id_Race);
CREATE UNIQUE INDEX UQ_Weapon_Class ON Weapon_Class(Id_Class, Id_Weapon);
CREATE UNIQUE INDEX UQ_Character_Build ON Character_Build(Id_CSR,Id_Player,Char_Name);
CREATE UNIQUE INDEX UQ_Player_Team ON Player_Team(Id_Team, Id_Player);
CREATE UNIQUE INDEX UQ_Game ON Game(Id_GM,Id_Map,Id_Event);
CREATE UNIQUE INDEX UQ_Game_Team ON Game_Team( Id_PT, Id_Game);
CREATE UNIQUE INDEX UQ_SM_SK ON SM_SK( Id_SK1, Id_SK2, Id_SK3);

ALTER TABLE Player ADD CONSTRAINT Nationality_Player_fk
FOREIGN KEY (Id_Nation)
REFERENCES Nationality (Id_Nation)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Class_Spec_Rac ADD CONSTRAINT Race_Class_Spec_fk
FOREIGN KEY (Id_Race)
REFERENCES Race (Id_Race)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Player_Team ADD CONSTRAINT Team_Player_Team_fk
FOREIGN KEY (Id_Team)
REFERENCES Team (Id_Team)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Sword ADD CONSTRAINT Sword_Kind_Sword_fk
FOREIGN KEY (Id_SK)
REFERENCES Sword_Kind (Id_SK)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE SM_SK ADD CONSTRAINT Spell_Kind_SM_SK_fk
FOREIGN KEY (Id_SK1)
REFERENCES Spell_Kind (Id_SK)
ON DELETE NO ACTION
ON UPDATE NO ACTION

ALTER TABLE SM_SK ADD CONSTRAINT Spell_Kind_SM_SK_fk1
FOREIGN KEY (Id_SK2)
REFERENCES Spell_Kind (Id_SK)
ON DELETE NO ACTION
ON UPDATE NO ACTION

ALTER TABLE SM_SK ADD CONSTRAINT Spell_Kind_SM_SK_fk2
FOREIGN KEY (Id_SK3)
REFERENCES Spell_Kind (Id_SK)
ON DELETE NO ACTION
ON UPDATE NO ACTION

ALTER TABLE Spec_SM ADD CONSTRAINT SM_SK_Spec_SM_fk
FOREIGN KEY (Id_SM_SK)
REFERENCES SM_SK (Id_SM_SK)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Class_Spec_Rac ADD CONSTRAINT Class_Class_Spec_fk
FOREIGN KEY (Id_Class)
REFERENCES Class (Id_Class)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Weapon_Class ADD CONSTRAINT Class_Weapon_Class_fk
FOREIGN KEY (Id_Class)
REFERENCES Class (Id_Class)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Event ADD CONSTRAINT Event_Type_Event_fk
FOREIGN KEY (Id_ET)
REFERENCES Event_Type (Id_ET)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Game ADD CONSTRAINT Event_Game_fk
FOREIGN KEY (Id_Event)
REFERENCES Event (Id_Event)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Game ADD CONSTRAINT Game_Mode_Game_fk
FOREIGN KEY (Id_GM)
REFERENCES Game_Mode (Id_GM)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Spec_SM ADD CONSTRAINT Specialization_Spec_SM_fk
FOREIGN KEY (Id_Spec)
REFERENCES Specialization (Id_Spec)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Class_Spec_Rac ADD CONSTRAINT Spec_SM_Class_Spec_Rac_fk
FOREIGN KEY (Id_SpecSM)
REFERENCES Spec_SM (Id_SpecSM)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Character_Build ADD CONSTRAINT Character_Build__Class_Spec_fk
FOREIGN KEY (Id_CSR)
REFERENCES Class_Spec_Rac (Id_CSR)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Bow ADD CONSTRAINT Weapons_Bow_fk
FOREIGN KEY (Id_Weapon)
REFERENCES Weapon (Id_Weapon)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Shield ADD CONSTRAINT Weapons_Shield_fk
FOREIGN KEY (Id_Weapon)
REFERENCES Weapon (Id_Weapon)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Sword ADD CONSTRAINT Weapons_Sword_fk
FOREIGN KEY (Id_Weapon)
REFERENCES Weapon (Id_Weapon)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Staff ADD CONSTRAINT Weapons_Staff_fk
FOREIGN KEY (Id_Weapon)
REFERENCES Weapon (Id_Weapon)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Weapon_Class ADD CONSTRAINT Weapon_Weapon_Class_fk
FOREIGN KEY (Id_Weapon)
REFERENCES Weapon (Id_Weapon)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Player_Team ADD CONSTRAINT Player_Player_Team_fk
FOREIGN KEY (Id_Player)
REFERENCES Player (Id_Player)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Character_Build ADD CONSTRAINT Player_Character_Build__fk
FOREIGN KEY (Id_Player)
REFERENCES Player (Id_Player)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Game_Team ADD CONSTRAINT Player_Team_Game_Team_fk
FOREIGN KEY (Id_PT)
REFERENCES Player_Team (Id_PT)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Game ADD CONSTRAINT Map_Game_fk
FOREIGN KEY (Id_Map)
REFERENCES Map (Id_Map)
ON DELETE CASCADE
ON UPDATE CASCADE

ALTER TABLE Game_Team ADD CONSTRAINT Game_Game_Team_fk
FOREIGN KEY (Id_Game)
REFERENCES Game (Id_Game)
ON DELETE CASCADE
ON UPDATE CASCADE