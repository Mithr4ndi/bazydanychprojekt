use s13883;

ALTER TABLE Player DROP CONSTRAINT Nationality_Player_fk;
ALTER TABLE Class_Spec_Rac DROP CONSTRAINT Race_Class_Spec_fk;
ALTER TABLE Player_Team DROP CONSTRAINT Team_Player_Team_fk;
ALTER TABLE Sword DROP CONSTRAINT Sword_Kind_Sword_fk;
ALTER TABLE SM_SK DROP CONSTRAINT Spell_Kind_SM_SK_fk
ALTER TABLE SM_SK DROP CONSTRAINT Spell_Kind_SM_SK_fk1
ALTER TABLE SM_SK DROP CONSTRAINT Spell_Kind_SM_SK_fk2
ALTER TABLE Spec_SM DROP CONSTRAINT SM_SK_Spec_SM_fk;
ALTER TABLE Class_Spec_Rac DROP CONSTRAINT Class_Class_Spec_fk;
ALTER TABLE Weapon_Class DROP CONSTRAINT Class_Weapon_Class_fk;
ALTER TABLE Event DROP CONSTRAINT Event_Type_Event_fk;
ALTER TABLE Game DROP CONSTRAINT Event_Game_fk;
ALTER TABLE Game DROP CONSTRAINT Game_Mode_Game_fk;
ALTER TABLE Spec_SM DROP CONSTRAINT Specialization_Spec_SM_fk;
ALTER TABLE Class_Spec_Rac DROP CONSTRAINT Spec_SM_Class_Spec_Rac_fk;
ALTER TABLE Character_Build DROP CONSTRAINT Character_Build__Class_Spec_fk;
ALTER TABLE Bow DROP CONSTRAINT Weapons_Bow_fk;
ALTER TABLE Shield DROP CONSTRAINT Weapons_Shield_fk;
ALTER TABLE Sword DROP CONSTRAINT Weapons_Sword_fk;
ALTER TABLE Staff DROP CONSTRAINT Weapons_Staff_fk;
ALTER TABLE Weapon_Class DROP CONSTRAINT Weapon_Weapon_Class_fk;
ALTER TABLE Player_Team DROP CONSTRAINT Player_Player_Team_fk;
ALTER TABLE Character_Build DROP CONSTRAINT Player_Character_Build__fk;
ALTER TABLE Game_Team DROP CONSTRAINT Player_Team_Game_Team_fk;
ALTER TABLE Game DROP CONSTRAINT Map_Game_fk;
ALTER TABLE Game_Team DROP CONSTRAINT Game_Game_Team_fk;

DROP TABLE nationality ;
DROP TABLE Race ;
DROP TABLE Team ;
DROP TABLE Spell_Kind ;
DROP TABLE SM_SK ;
DROP TABLE Class ;
DROP TABLE Event_Type ;
DROP TABLE Event ;
DROP TABLE Game_Mode ;
DROP TABLE Specialization ;
DROP TABLE Spec_SM ;
DROP TABLE Class_Spec_Rac ;
DROP TABLE Weapon ;
DROP TABLE Weapon_Class ;
DROP TABLE Staff ;
DROP TABLE Sword ;
DROP TABLE Sword_Kind ;
DROP TABLE Shield ;
DROP TABLE Bow ;
DROP TABLE Player ;
DROP TABLE Character_Build;
DROP TABLE Map ;
DROP TABLE Game ;
DROP TABLE Game_Team ;
DROP TABLE Player_Team ;

 