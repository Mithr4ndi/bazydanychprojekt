SET DATEFORMAT dmy;

--1. Dane graczy(Name, Last_Name, Account_Name, Brirth_Date, Mail, Nationality) pochodzacych z Hiszpanii
SELECT Name, Last_Name, Account_Name, Birth_Date, Mail, Nationality
FROM Player
JOIN Nationality ON Player.Id_Nation = Nationality.Id_Nation
WHERE Nationality = 'Spain';

--2. Ilosc graczy, ktorzy graja wzajemnie roznymi klasami
SELECT COUNT(DISTINCT Id_Class) "L. GRACZY"
FROM Character_Build
JOIN Class_Spec_Rac ON Character_Build.Id_CSR = Class_Spec_Rac.Id_CSR;

--3. Nazwy kont i maile graczy oraz nazwy ich postaci posiadajach bron Shadowmourne lub Frostmourne, posortowac po Weapon_Name. 
SELECT Account_Name, Mail, Weapon_Name, Char_Name
FROM Character_Build
JOIN Player ON Character_Build.Id_Player = Player.Id_Player
JOIN Class_Spec_Rac ON Character_Build.Id_CSR = Class_Spec_Rac.Id_CSR
JOIN Class ON Class_Spec_Rac.Id_Class = Class.Id_Class
JOIN Weapon_Class ON Class.Id_Class = Weapon_Class.Id_Class
JOIN Weapon ON Weapon_Class.Id_Weapon = Weapon.Id_Weapon
WHERE Weapon_Name = 'Shadowmourne' OR Weapon_Name = 'Frostmourne'
ORDER BY Weapon_Name;

--4. Znajdz dwa luki zadajace najwiecej obrazen.
SELECT * 
FROM Bow b
WHERE 2 > (SELECT COUNT(*)
            FROM Bow
            WHERE b.damage < damage);
 
--5. Procent poszczegolnych ras, ktorymi graja gracze           
SELECT Id_Race, ilosc_granych / rasy * 100
FROM(SELECT CAST(COUNT(*) AS FLOAT )  AS rasy FROM Race)c,
(SELECT Id_Race, CAST(COUNT(*) AS FLOAT ) AS ilosc_granych
FROM Class_Spec_Rac
GROUP BY Id_Race) d;

--6. Wypisac nazwe postaci, nazwe specjalizacji, nazwe konta i posortowac wyniki wedlug nazwy konta potem specjalizacji malejaco. 
SELECT Char_Name, Spec_Name, Account_Name
FROM Character_Build
JOIN Player ON Character_Build.Id_Player = Player.Id_Player
JOIN Class_Spec_Rac ON Character_Build.Id_CSR = Class_Spec_Rac.ID_CSR
JOIN Spec_SM ON Spec_SM.Id_SpecSM = Class_Spec_Rac.Id_SpecSM
JOIN Specialization ON Spec_SM.Id_Spec = Specialization.Id_Spec
ORDER BY Account_Name, Spec_Name DESC;

--7. Znajdz uzytkownika, ktory ma mail w domenie dolor.org
SELECT *
FROM Player
WHERE mail like '%dolor.org';

--8. Znajdz bron typu Staff, ktorej maksymalna ilosc obrazen(Damage) jest wieksza niz srednia obrazen broni typu Sword
SELECT Weapon_Name, MAX(Damage)
FROM Weapon
JOIN Staff ON Weapon.Id_Weapon = Staff.Id_Weapon
GROUP BY Weapon_Name
HAVING MAX(Damage) > (Select AVG(damage) FROM Sword);


--9. Dane pierwszych zarejestrowanych graczy i ostatnio zarejestrowanych graczy
SELECT * 
FROM Player 
WHERE Register_Date >= '01/01/04'
UNION ALL
SELECT * 
FROM Player 
WHERE  Register_Date >= '31/12/17';

--10. Wypisz nazwe konta gracza, ktore jest zar�wno w teamie o nazwie 'bibendum. Donec' jak i 'elit, pellentesque'
SELECT Account_Name
FROM Player 
JOIN Player_Team ON Player.Id_Player = Player_Team.Id_Player
JOIN Team ON Player_Team.ID_Team = Team.Id_Team
WHERE Team_Name = 'bibendum. Donec'
INTERSECT
SELECT Account_Name
FROM Player 
JOIN Player_Team ON Player.Id_Player = Player_Team.Id_Player
JOIN Team ON Player_Team.ID_Team = Team.Id_Team
WHERE Team_Name = 'elit, pellentesque';


--11. Wypisac id gier, nazwe trybu gry, ktore rozgrywaja sie na mapie Altretic Valley, bez wypisanai trybu Normal Death Match.
Select Id_game, GM_Name AS Tryb, Map_Name 
FROM Game
join Game_mode ON Game.id_gm = Game_mode.ID_GM 
JOIN Map ON Map.id_map = Map.ID_map 
WHERE Map_Name = 'Altretic Valley'
EXCEPT
Select Id_game, GM_Name, Map_Name
FROM Game
join Game_mode ON Game.id_gm = Game_mode.ID_GM 
JOIN Map ON Map.id_map = Map.ID_map 
WHERE GM_Name = 'Normal Death Match';

--12. Wybierz ID i nazwe tarczy oraz Stamine, ktora miesci sie w przedziale miedzy 150 a 350
SELECT Weapon.Id_Weapon, Weapon_Name, Stamina
FROM Weapon
JOIN Shield ON Weapon.Id_Weapon = Shield.Id_Weapon
WHERE Stamina BETWEEN 150 AND 350;

--13. Wybierz nazwy postaci, ktorych nazwy sa szescioliterowe oraz ich id
SELECT Char_name, Id_Character
FROM Character_build
WHERE LEN(Char_name) = 6;

--14. Wybierz nazwy lukow, ktorych haste_rating jest wiekszy od haste_rating dwolnego luku posuidajacego Attack_power miedzy 70 a 80.
SELECT Weapon.Id_Weapon, Weapon_Name, Haste_Rating, Attack_Power
FROM Weapon
JOIN Bow ON Weapon.Id_Weapon = Bow.Id_Weapon
WHERE Haste_Rating > ANY (SELECT Haste_Rating
                            FROM Weapon
                            JOIN Bow ON Weapon.Id_Weapon = Bow.Id_Weapon
                            WHERE  Attack_power BETWEEN 70 AND 80);

--15. Wypisz nazwe broni, ktorych zadawane obrazenia sa mniejsze od sredniej obrazen zadawanych przez wszystkei miecze rodzaju One Hand.                       
SELECT  Weapon_Name, Damage
FROM Weapon
JOIN Sword ON Weapon.Id_Weapon = Sword.Id_Weapon
WHERE Damage < ALL(SELECT AVG(Damage)
                            FROM Sword
                            JOIN Sword_Kind ON Sword.Id_SK = Sword_Kind.Id_SK
                            WHERE Kind = 'One Hand');
                         
--16. Sprawdz czy istnieja gracze, ktorzy nie naleza do zadnego teamu.                            
SELECT Id_Player
FROM Player p
WHERE NOT EXISTS(SELECT Id_Player FROM Player_Team t WHERE t.Id_Player = p.Id_Player);
 
