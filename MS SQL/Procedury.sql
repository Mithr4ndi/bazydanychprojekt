use s13883;
SET DATEFORMAT dmy;
GO


/* Procedura tworzaca gre na podstawie podanych parametorw, ktorymi sa nazwy trybu gry i mapy oraz przypisac 
im obecnie trwajacy event. Jesli trwa obecnie wiecej niz jeden Event wybierany jest ten ktory zaczal sie wczesniej.
 W przypadku, gdy istnieje w bazie gra o podanych parametrach zostanie podniesiony blad ze stosownym komunikatem. */
 
--ALTER
CREATE 
PROCEDURE createGame @Gm Varchar(40), @Map Varchar(40)
AS
DECLARE
@IdGM INT, 
@IdM INT,
@IdE INT;
BEGIN TRY
SELECT TOP 1 @IdE = (Id_Event) FROM Event 
WHERE DAY(Start_Date) <= DAY(GETDATE())AND Stop_Date >= GETDATE() AND MONTH(Start_Date) = MONTH(GETDATE())
ORDER BY Start_Date DESC; 
SELECT @IdGM = Id_GM FROM Game_Mode WHERE GM_Name = @Gm;
SELECT @IdM  = Id_Map FROM Map WHERE Map_Name = @Map;

IF NOT EXISTS (SELECT 1 FROM Game WHERE Id_GM = @IdGM AND Id_Map = @IdM AND Id_Event = @IdE)
BEGIN
INSERT INTO Game(Id_GM, Id_Map, Id_Event) VALUES (@IdGM, @IdM, @IdE);
SELECT Id_Game, GM_Name, Map_Name, Event_Name FROM Game
JOIN Game_Mode ON Game_Mode.Id_GM = Game.Id_GM
JOIN Map ON Map.Id_Map = Game.Id_Map
JOIN Event ON Event.Id_Event = Game.Id_Event
WHERE Id_Game = @@IDENTITY;
END;
ELSE
RAISERROR('Gra o podanych parametrach istnieje juz w bazie', 1, 10);
END TRY
BEGIN CATCH DECLARE @ErrMsg NVARCHAR(100), @ErrSev INT, @ErrSt INT; 
		SELECT	@ErrMsg = ERROR_MESSAGE() ,
				@ErrSev = ERROR_SEVERITY() ,
				@ErrSt = ERROR_STATE(); 
Print @ErrMsg + ' Severity = ' + Cast(@ErrSev As Varchar(3)) + ' State = ' + Cast(@ErrSt As Varchar(3)); 
END CATCH;
GO
-------------createGame--------------------------------------------------------
EXEC createGame 'Battleground', 'Eye of Storm';
EXEC createGame 'Battleground', 'Eye of Storm';
--------------------------------------------------------------------------------
GO

/*Utworzenie profilu postaci. Procedura otrzymuje w parametrze nazwe istniejace konta, nazwe tworzonej postaci,
klase, rase, specializacje oraz 3 rodzaje umeiejetnosci, nastepnie weryfikuje czy konto o podanej nazwie istnieje w bazie
 oraz czy podana nazwa postaci nie jest zajeta, potem dodaje do bazy postac wypisujac po kazdej operacji stosowna informacje. 
 W przypadku gdy konto nie istniej lub nazwa postaci jest zajeta zostaje podniesiony blad.*/

--ALTER
CREATE
PROCEDURE createCharacter @AccountName Varchar(40), @CharName Varchar(40), @Race Varchar(40),
						  @Class Varchar(40), @Specialization Varchar(40), 
						  @Spell1 Varchar(20), @Spell2 Varchar(20), @Spell3 Varchar(20)
AS
DECLARE
@Id_aN INT,
@Id_Rac INT,
@Id_Class INT,
@Id_Spec INT,
@Id_S1 INT,
@Id_S2 INT,
@Id_S3 INT,
@Id INT;
BEGIN TRY
IF  NOT EXISTS (SELECT 1 FROM Player WHERE Account_Name = @AccountName)
	BEGIN
	RAISERROR('Konto nie istnieje!', 1, 10);
	END;
ELSE
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM Character_Build WHERE Char_Name = @CharName)
		BEGIN

		SELECT @Id_aN = Id_Player FROM Player WHERE Account_Name = @AccountName;
		SELECT @Id_Rac = Id_Race FROM Race WHERE R_Name = @Race;
		SELECT @Id_Class = Id_Class FROM Class WHERE Class_Name = @Class;
		SELECT @Id_Spec = Id_Spec FROM Specialization WHERE Spec_Name = @Specialization;
		SELECT @Id_S1 = Id_SK FROM Spell_Kind WHERE Short_Name = @Spell1;
		SELECT @Id_S2 = Id_SK FROM Spell_Kind WHERE Short_Name = @Spell2;
		SELECT @Id_S3 = Id_SK FROM Spell_Kind WHERE Short_Name = @Spell3;

		INSERT INTO SM_SK(Id_SK1, Id_SK2, Id_SK3) VALUES (@Id_S1, @Id_S2, @Id_S3);
		SET @Id = @@IDENTITY;
		PRINT 'Dodano zestaw umiejetnosci: ' + @Spell1 + ' ' + @Spell2 + ' ' + @Spell3 + '!';
		INSERT INTO Spec_SM(Id_SM_SK, Id_Spec) VALUES (@Id, @Id_Spec);
		SET @Id = @@IDENTITY;
		PRINT 'Dodano specjalizacje: ' + @Specialization +'!';
		INSERT INTO Class_Spec_Rac(Id_Class, Id_SpecSM, Id_Race) VALUES (@Id_Class, @Id, @Id_Rac);
		SET @Id = @@IDENTITY;
		PRINT 'Dodano rase: ' + @Race + ' i klase: ' + @Class +'!';
		INSERT INTO Character_Build(Id_CSR, Id_Player, Char_Name) VALUES (@Id, @Id_aN, @CharName);
		SET @Id = @@IDENTITY;
		PRINT 'Postac: ' + @CharName + ' zostala utworzona dla konta: ' + @AccountName + '!';

		END
		ELSE
		RAISERROR('Postac o podanej nazwie juz istnieje!', 2, 11);
	END
END TRY
BEGIN CATCH DECLARE @ErrMsg NVARCHAR(100), @ErrSev INT, @ErrSt INT; 
		SELECT	@ErrMsg = ERROR_MESSAGE() ,
				@ErrSev = ERROR_SEVERITY() ,
				@ErrSt = ERROR_STATE(); 
Print @ErrMsg + ' Severity = ' + Cast(@ErrSev As Varchar(3)) + ' State = ' + Cast(@ErrSt As Varchar(3)); 
END CATCH;
GO
-------------createCharacter----------------------------------------------------
EXEC createCharacter 'sollicitudin', 'Soto', 'Night Elf', 'Death Knight', 'Tank', 'HOT', 'DOT', 'BS';
EXEC createCharacter 'elit', 'Soto', 'Night Elf', 'Death Knight', 'Tank', 'HOT', 'DOT', 'BS';
EXEC createCharacter 'Beastialski', 'Soto', 'Night Elf', 'Death Knight', 'Tank', 'HOT', 'DOT', 'BS';
EXEC createCharacter 'Quisque', 'Stereo', 'Night Elf', 'Death Knight', 'Tank', 'HOT', 'DOT', 'BS';
--------------------------------------------------------------------------------
GO

/* Procedura usuwa nieaktywne konta(takie na ktorych ostatnie logowanie bylo zarejestrowane ponad 150 dni temu)
jesli napotka po drodze konta, ktore byly aktywne zgolsi blad i konto nie zostanie usuniete.
Procedura wypisuje rowniez stosowne komunikaty informujace, ze konto zostalo lub nie usuniete. */

ALTER
--CREATE
PROCEDURE deletePlayer
AS
DECLARE 
@Id_Player INT,
@Last_online DATE,
@Account_Name VARCHAR(64);
DECLARE curPlayer CURSOR FOR SELECT Id_Player, Last_online, Account_Name FROM Player;
BEGIN
OPEN curPlayer;
FETCH NEXT FROM curPlayer INTO @Id_Player,@Last_online, @Account_Name;
WHILE @@FETCH_STATUS = 0
BEGIN

	IF DATEDIFF(dd, @Last_online, GETDATE()) > 150
	BEGIN
	DELETE FROM Player WHERE Id_Player = @Id_Player;
	PRINT 'Usunieto gracza: ' + @Account_Name;
	END
	ELSE
	BEGIN
	PRINT 'Konto: ' + @Account_Name + ' nie zostalo usuniete!';
	RAISERROR('Nie mozna usunac aktywnego konta. Ostatnie logowanie bylo wykonane w ciagu ostatnich 150 dni!', 1, 10);
	END

FETCH NEXT FROM curPlayer INTO @Id_Player,@Last_online,@Account_Name;
END;
CLOSE curPlayer;
DEALLOCATE curPlayer;
END;
GO

-------------deletePlayer----------------------------------------------------
EXEC deletePlayer;
SELECT * FROM Player;
--------------------------------------------------------------------------------
