SET SERVEROUTPUT ON;
--1. Dodawanie nowego gracza do bazy.
--ZALOZENIA:
-- * posiadanie kilku kont przez danego gracza jest dozwolone, musza miec jednak one inna nazwe
-- * data rejestracji jest data utworzenia konta, ostatni raz online jest rowniez data utworzenia konta
-- * w przypadku gdy nazwa konta bedzie istaniala juz w bazie zostanie podniesiony wyjatek
-- * komunikat o dodaniu gracza do bazy

CREATE OR REPLACE PROCEDURE addPlayer (v_Name Player.Name%Type, 
                                       v_LastName Player.Last_Name%Type,
                                       v_Account Player.Account_Name%Type,
                                       v_Password Player.Password%Type,
                                       v_Nationality Nationality.Nationality%Type,
                                       v_Birth_Date Player.Birth_Date%Type,
                                       v_Mail Player.Mail%Type)
AS
v_IdP NUMBER;
v_Nation NUMBER;
v_aN NUMBER;
e_alreadyExists EXCEPTION;

BEGIN
    SELECT COUNT(1) INTO v_aN FROM Player WHERE Account_Name = v_Account;
    SELECT Id_Nation INTO v_Nation FROM Nationality WHERE Nationality = v_Nationality;
    SELECT MAX(Id_Player) + 1 INTO v_IdP FROM Player;
    
    
IF v_aN > 0 THEN
    RAISE e_alreadyExists;
ELSE
    INSERT INTO Player VALUES (v_IdP, v_Name, v_LastName, v_Account, v_Password, v_Nation, v_Birth_Date, v_Mail, SYSDATE, SYSDATE);
    dbms_output.put_line('Gracz ' || v_Account || ' zostal dodany do bazy.');
END IF;

EXCEPTION 
    WHEN e_alreadyExists THEN
    dbms_output.put_line('W bazie istnieje juz konto ' || v_Account);
END;


 
--2. Procedura sprawdzajaca daty rozpoczecia eventow, jesli dany event juz wystartowal zmienia jego date na przyszloroczna.
-- Procedura sprawdza rowniez date jego zakonczenia i jesli ta minela, ja takze zmienia na przyszloroczna oraz zliczy
-- ilosc zaaktualizowanych rekordow.

CREATE OR REPLACE PROCEDURE changeYearEvent
AS
v_counter NUMBER;
BEGIN
v_counter := 0;
DECLARE
CURSOR curEvent IS SELECT Id_Event, Stop_Date, Start_Date FROM Event WHERE Start_Date <= SYSDATE;
v_IdE NUMBER;
v_Stop Event.Stop_Date%Type;
v_Start Event.Start_Date%Type;
BEGIN
OPEN curEvent;
    LOOP
    FETCH curEvent INTO v_IdE, v_Stop, v_Start;
    EXIT WHEN curEvent%NOTFOUND;
    
    UPDATE Event SET Start_Date = ADD_MONTHS(v_Start, 12)  WHERE Id_Event = v_IdE;
    v_counter :=  v_counter + 1;
    
    IF v_Stop <= SYSDATE THEN
    UPDATE Event SET Stop_Date = ADD_MONTHS(v_Start, 12)  WHERE Id_Event = v_IdE;
    v_counter :=  v_counter + 1;
    END IF;
    
    END LOOP;
    CLOSE curEvent;    
    dbms_output.put_line('Zaaktualizowano ' || v_counter|| ' rekordow.');
END;
END;

--3.Zmiana hasla gracza. Procedura musi sprawdzic czy podane konto istnieje, nastepnie czy nowe haslo jest inne niz stare
-- oraz czy zawiera minimum 8 znakow w tym conajmniej 1 cyfre. 

CREATE OR REPLACE PROCEDURE changePassword (v_Account VARCHAR2, v_newPass VARCHAR2)
AS
v_oldPass VARCHAR2(40);
v_regex VARCHAR2(40);
v_acc INT;
v_check NUMBER;
e_samePass EXCEPTION;
e_accNotFound EXCEPTION;
e_NoReq EXCEPTION;
BEGIN
SELECT COUNT(1) INTO v_acc FROM Player WHERE Account_Name = v_Account;

IF v_acc = 0 THEN
RAISE e_accNotFound;
END IF; 
 
SELECT  REGEXP_INSTR(to_char(v_newPass), '(\d+\w{7,})|(\w{7,}\d)|(\w{6,}\d\w{1,})|(\w{5,}\d\w{2,})|(\w{4,}\d\w{3,})|(\w{3,}\d\w{4,})|(\w{2,}\d\w{5,})|(\w{1,}\d\w{6,})') INTO v_check FROM DUAL;
SELECT Password INTO v_oldPass FROM Player WHERE Account_Name = v_Account;

    IF v_oldPass = v_newPass THEN
        RAISE e_samePass;
    ELSIF v_check = 1  THEN
        UPDATE Player SET Password = v_newPass WHERE Account_Name = v_Account;
        dbms_output.put_line('Haslo zmienione.');
    ELSE 
        RAISE e_NoReq;
   END IF;
   
EXCEPTION 
    WHEN e_accNotFound THEN
    dbms_output.put_line('Podane konto nie istnieje.');
    WHEN e_samePass THEN
    dbms_output.put_line('Hasla musza sie roznic. Podane haslo jest takie samo jak poprzednie');
    WHEN e_NoReq THEN
    dbms_output.put_line('Podane haslo nie spelnia wymagan. Haslo musi zawierac minimum 8 znakow w tym conajmniej jedna cyfre.');
END;



  