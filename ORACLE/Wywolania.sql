SET SERVEROUTPUT ON;

----------------------------PROCEDURY-------------------------------------------

--------------addPlayer---------------------------------------------------------
CALL addPlayer('Jack', 'Sparrow', 'BlackPearl', 'BarbossaNoob', 'Poland', '14/08/90', 'jack888@disney.com');
CALL addPlayer('Hektor', 'Barbossa', 'BlackPearl', 'SparrowNoob', 'Poland', '15/08/90', 'hektor121@disney.com');

-------------changeYearEvent----------------------------------------------------
CALL changeYearEvent();

-------------changePassword-----------------------------------------------------
CALL changePassword('BlackPearl', 'BarbossaNoob');
CALL changePassword('Salazar123', 'abc123def');
CALL changePassword('BlackPearl', 'abcd1456qw');


---------------------TESTY TRIGGEROW--------------------------------------------

-----------------agePlayer------------------------------------------------------
INSERT INTO Player VALUES (12, 'William', 'Turner', 'ThePirate', 'haselo456', 4, '05/07/08', 'will.T@disney.com', '13/06/17', '13/06/17');

------------------DefShield-----------------------------------------------------
INSERT INTO Shield VALUES (31, 521, 234, 82, 55);
UPDATE Shield SET Defense_Rating = 150 WHERE Id_Weapon = 22;

-----------------noInsertSpellMode----------------------------------------------
INSERT INTO Specialization VALUES (4, 'Specialization 4');
