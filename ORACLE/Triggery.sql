SET SERVEROUTPUT ON;
--1. Trigger sprawdzajacy czy gracz dodawany do bazy ma ukonczone 18 lat. 
CREATE OR REPLACE TRIGGER agePlayer
BEFORE INSERT ON PLAYER
FOR EACH ROW
BEGIN
IF ((SYSDATE - :new.Birth_Date) < 18*365) THEN
RAISE_APPLICATION_ERROR(-20000,'Gracz nie ma ukonczonych 18 lat!');
END IF;
END;

--2. Trigger, ktory przy wstawianiu nowego rekordu lub aktualizowaniu w tabeli Shield, sprawdzi nowa wartosc Defense Rating. 
--Przy wstawianiu: jesli wartosc DR <= 100 a wartosc Staminy przekracza 500 nie dopusci do wstawienia rekordu. 
-- Przy aktualizowaniu roznica pomiedzy nowa wartoscia DR a stara nie moze przekraczac 100.
CREATE OR REPLACE TRIGGER DefShield
BEFORE UPDATE OR INSERT ON Shield
FOR EACH ROW 
BEGIN
IF Inserting THEN
    IF (:new.Defense_Rating <= 100 AND :new.Stamina > 500 )THEN
    RAISE_APPLICATION_ERROR(-20000,'Przy Defense Rating mniejszym rownym 100 Stamina nie moze przekroczyc wartosci 500!');
    END IF;   
END IF;

IF Updating THEN
 IF (:new.Defense_Rating - :old.Defense_Rating) > 100 OR (:old.Defense_Rating -:new.Defense_Rating) > 100 THEN
  :new.Defense_Rating  := :old.Defense_Rating; 
  dbms_output.put_line('Update nie zostal wykonany! Wartosc nie moze przekraczac 100!');
     END IF;   
END IF;
END;

--3. Trigger nie pozwalajacy wstawic nowej spejalizacji, jesli sa juz 3. 
CREATE OR REPLACE TRIGGER noInsertSpellMode
BEFORE INSERT ON Specialization
DECLARE
v_counter NUMBER;
BEGIN
SELECT COUNT(*) INTO v_counter FROM Specialization;
IF (v_counter = 3) THEN
RAISE_APPLICATION_ERROR(-20000,'Nie mozna dodac nowej sepcjalizacji!');
END IF;
END;


