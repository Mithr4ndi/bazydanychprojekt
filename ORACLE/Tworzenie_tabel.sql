
CREATE TABLE Nationality (
                Id_Nation NUMBER NOT NULL,
                Nationality VARCHAR2(40) NOT NULL,
                CONSTRAINT NATION_PK PRIMARY KEY (Id_Nation)
);


CREATE TABLE Race (
                Id_Race NUMBER NOT NULL,
                R_Name VARCHAR2(40) NOT NULL,
                CONSTRAINT RACE_PK PRIMARY KEY (Id_Race)
);


CREATE TABLE Team (
                Id_Team NUMBER NOT NULL,
                Team_Name VARCHAR2(40) NOT NULL,
                CONSTRAINT TEAM_PK PRIMARY KEY (Id_Team)
);

CREATE TABLE Sword_Kind (
                Id_SK NUMBER NOT NULL,
                Kind VARCHAR2(40) NOT NULL,
                CONSTRAINT SWORD_KIND_PK PRIMARY KEY (Id_SK)
);

CREATE TABLE Spell_Kind (
                Id_SK NUMBER NOT NULL,
                Full_Name VARCHAR2(40) NOT NULL,
                Short_Name VARCHAR2(40) NOT NULL,
                CONSTRAINT SPELL_KIND_PK PRIMARY KEY (Id_SK)
);



CREATE TABLE SM_SK (
                Id_SM_SK NUMBER NOT NULL,
                Id_SK1 NUMBER NOT NULL,
                Id_SK2 NUMBER NOT NULL,
                Id_SK3 NUMBER NOT NULL,
                CONSTRAINT SM_SK_PK PRIMARY KEY (Id_SM_SK),
                UNIQUE(Id_SK1,Id_SK2,Id_SK3)
);


CREATE TABLE Class (
                Id_Class NUMBER NOT NULL,
                Class_Name VARCHAR2(40) NOT NULL,
                CONSTRAINT CLASS_PK PRIMARY KEY (Id_Class)
);


CREATE TABLE Event_Type (
                Id_ET NUMBER NOT NULL,
                ET_Name VARCHAR2(40) NOT NULL,
                Description VARCHAR2(40) NOT NULL,
                CONSTRAINT EVENT_TYPE_PK PRIMARY KEY (Id_ET)
);


CREATE TABLE Event (
                Id_Event NUMBER NOT NULL,
                Id_ET NUMBER NOT NULL,
                Event_Name VARCHAR2(40) NOT NULL,
                Stop_Date DATE NOT NULL,
                Start_Date DATE NOT NULL,
                CONSTRAINT EVENT_PK PRIMARY KEY (Id_Event)
);


CREATE TABLE Game_Mode (
                Id_GM NUMBER NOT NULL,
                GM_Name VARCHAR2(40) NOT NULL,
                Description VARCHAR2(40) NOT NULL,
                CONSTRAINT GAME_MODE_PK PRIMARY KEY (Id_GM)
);


CREATE TABLE Specialization (
                Id_Spec NUMBER NOT NULL,
                Spec_Name VARCHAR2(40) NOT NULL,
                CONSTRAINT SPECIALIZATION_PK PRIMARY KEY (Id_Spec)
);


CREATE TABLE Spec_SM (
                Id_SpecSM NUMBER NOT NULL,
                Id_SM_SK NUMBER NOT NULL,
                Id_Spec NUMBER NOT NULL,
                CONSTRAINT SPEC_SM_PK PRIMARY KEY (Id_SpecSM),
                UNIQUE( Id_SM_SK, Id_Spec)
);


CREATE TABLE Class_Spec_Rac (
                Id_CSR NUMBER NOT NULL,
                Id_Class NUMBER NOT NULL,
                Id_SpecSM NUMBER NOT NULL,
                Id_Race NUMBER NOT NULL,
                CONSTRAINT CLASS_SPEC_RAC_PK PRIMARY KEY (Id_CSR),
                UNIQUE(Id_Class, Id_SpecSM, Id_Race)
);


CREATE TABLE Weapon (
                Id_Weapon NUMBER NOT NULL,
                Weapon_Name VARCHAR2(40) NOT NULL,
                CONSTRAINT WEAPON_PK PRIMARY KEY (Id_Weapon)
);


CREATE TABLE Weapon_Class (
                Id_WeaponClass NUMBER NOT NULL,
                Id_Class NUMBER NOT NULL,
                Id_Weapon NUMBER NOT NULL,
                CONSTRAINT WEAPON_CLASS_PK PRIMARY KEY (Id_WeaponClass),
                UNIQUE(Id_Class, Id_Weapon)
);


CREATE TABLE Staff (
                Id_Weapon NUMBER NOT NULL,
                Damage NUMBER NOT NULL,
                Intellect NUMBER NOT NULL,
                Spirit NUMBER NOT NULL,
                Spell_Power NUMBER NOT NULL,
                CONSTRAINT STAFF_PK PRIMARY KEY (Id_Weapon)
);


CREATE TABLE Sword (
                Id_Weapon NUMBER NOT NULL,
                Id_SK NUMBER NOT NULL,
                Damage NUMBER NOT NULL,
                Strange NUMBER NOT NULL,
                Attack_Power NUMBER NOT NULL,
                Haste_Rating NUMBER NOT NULL,
                CONSTRAINT SWORD_PK PRIMARY KEY (Id_Weapon)
);


CREATE TABLE Shield (
                Id_Weapon NUMBER NOT NULL,
                Stamina NUMBER NOT NULL,
                Strange NUMBER NOT NULL,
                Defense_Rating NUMBER NOT NULL,
                Parry_Rating NUMBER NOT NULL,
                CONSTRAINT SHIELD_PK PRIMARY KEY (Id_Weapon)
);


CREATE TABLE Bow (
                Id_Weapon NUMBER NOT NULL,
                Damage NUMBER NOT NULL,
                Agility NUMBER NOT NULL,
                Attack_Power NUMBER NOT NULL,
                Haste_Rating NUMBER NOT NULL,
                CONSTRAINT BOW_PK PRIMARY KEY (Id_Weapon)
);


CREATE TABLE Player (
                Id_Player NUMBER NOT NULL,
                Name VARCHAR2(40) NOT NULL,
                Last_Name VARCHAR2(40) NOT NULL,
                Account_Name VARCHAR2(40) UNIQUE NOT NULL,
                Password VARCHAR2(40) NOT NULL,
                Id_Nation NUMBER NOT NULL,
                Birth_Date DATE NOT NULL,
                Mail VARCHAR2(40) NOT NULL,
                Register_Date DATE,
                Last_online DATE,
                CONSTRAINT PLAYER_PK PRIMARY KEY (Id_Player)
);


CREATE TABLE Character_Build  (
                Id_Character NUMBER NOT NULL,
                Id_CSR NUMBER NOT NULL,
                Id_Player NUMBER NOT NULL,
                Char_Name VARCHAR2(40) NOT NULL,
                CONSTRAINT CHARACTER_BUILD__PK PRIMARY KEY (Id_Character),
                UNIQUE(Id_CSR, Id_Player, Char_Name)
);


CREATE TABLE Player_Team (
                Id_PT NUMBER NOT NULL,
                Id_Team NUMBER NOT NULL,
                Id_Player NUMBER NOT NULL,
                CONSTRAINT PLAYER_TEAM_PK PRIMARY KEY (Id_PT),
                UNIQUE(Id_Team, Id_Player)
);


CREATE TABLE Map (
                Id_Map NUMBER NOT NULL,
                Map_Name VARCHAR2(40) NOT NULL,
                Number_of_players VARCHAR2(40) NOT NULL,
                CONSTRAINT MAP_PK PRIMARY KEY (Id_Map)
);


CREATE TABLE Game (
                Id_Game NUMBER NOT NULL,
                Id_GM NUMBER NOT NULL,
                Id_Map NUMBER NOT NULL,
                Id_Event NUMBER NOT NULL,
                CONSTRAINT GAME_PK PRIMARY KEY (Id_Game),
                UNIQUE(Id_GM,Id_Map,Id_Event)
);


CREATE TABLE Game_Team (
                Id_GameTeam NUMBER NOT NULL,
                Id_PT NUMBER NOT NULL,
                Id_Game NUMBER NOT NULL,
                CONSTRAINT GAME_TEAM_PK PRIMARY KEY (Id_GameTeam),
                UNIQUE( Id_PT, Id_Game)
);


ALTER TABLE Player ADD CONSTRAINT NATION_PLAYER_FK
FOREIGN KEY (Id_Nation)
REFERENCES Nationality (Id_Nation)
NOT DEFERRABLE;

ALTER TABLE Class_Spec_Rac ADD CONSTRAINT RACE_CLASS_SPEC_FK
FOREIGN KEY (Id_Race)
REFERENCES Race (Id_Race)
NOT DEFERRABLE;

ALTER TABLE Player_Team ADD CONSTRAINT TEAM_PLAYER_TEAM_FK
FOREIGN KEY (Id_Team)
REFERENCES Team (Id_Team)
NOT DEFERRABLE;

ALTER TABLE Sword ADD CONSTRAINT SWORD_KIND_SWORD_FK
FOREIGN KEY (Id_SK)
REFERENCES Sword_Kind (Id_SK)
NOT DEFERRABLE;

ALTER TABLE SM_SK ADD CONSTRAINT SPELL_KIND_SM_SK_FK
FOREIGN KEY (Id_SK1)
REFERENCES Spell_Kind (Id_SK)
NOT DEFERRABLE;


ALTER TABLE SM_SK ADD CONSTRAINT SPELL_KIND_SM_SK_FK1
FOREIGN KEY (Id_SK2)
REFERENCES Spell_Kind (Id_SK)
NOT DEFERRABLE;


ALTER TABLE SM_SK ADD CONSTRAINT SPELL_KIND_SM_SK_FK2
FOREIGN KEY (Id_SK3)
REFERENCES Spell_Kind (Id_SK)
NOT DEFERRABLE;

ALTER TABLE Class_Spec_Rac ADD CONSTRAINT CLASS_CLASS_SPEC_FK
FOREIGN KEY (Id_Class)
REFERENCES Class (Id_Class)
NOT DEFERRABLE;

ALTER TABLE Weapon_Class ADD CONSTRAINT CLASS_WEAPON_CLASS_FK
FOREIGN KEY (Id_Class)
REFERENCES Class (Id_Class)
NOT DEFERRABLE;

ALTER TABLE Event ADD CONSTRAINT EVENT_TYPE_EVENT_FK
FOREIGN KEY (Id_ET)
REFERENCES Event_Type(Id_ET)
NOT DEFERRABLE;

ALTER TABLE Game ADD CONSTRAINT EVENT_GAME_FK
FOREIGN KEY (Id_Event)
REFERENCES Event (Id_Event)
NOT DEFERRABLE;

ALTER TABLE Game ADD CONSTRAINT GAME_MODE_GAME_FK
FOREIGN KEY (Id_GM)
REFERENCES Game_Mode (Id_GM)
NOT DEFERRABLE;

ALTER TABLE Spec_SM ADD CONSTRAINT SPECIALIZATION_SPEC_SM_FK
FOREIGN KEY (Id_Spec)
REFERENCES Specialization (Id_Spec)
NOT DEFERRABLE;

ALTER TABLE Class_Spec_Rac ADD CONSTRAINT SPEC_SM_CLASS_SPEC_RAC_FK
FOREIGN KEY (Id_SpecSM)
REFERENCES Spec_SM (Id_SpecSM)
NOT DEFERRABLE;

ALTER TABLE Character_Build  ADD CONSTRAINT CHARACTER_BUILD__CLASS_SPEC_FK
FOREIGN KEY (Id_CSR)
REFERENCES Class_Spec_Rac (Id_CSR)
NOT DEFERRABLE;

ALTER TABLE Bow ADD CONSTRAINT WEAPONS_BOW_FK
FOREIGN KEY (Id_Weapon)
REFERENCES Weapon (Id_Weapon)
NOT DEFERRABLE;

ALTER TABLE Shield ADD CONSTRAINT WEAPONS_SHIELD_FK
FOREIGN KEY (Id_Weapon)
REFERENCES Weapon (Id_Weapon)
NOT DEFERRABLE;

ALTER TABLE Sword ADD CONSTRAINT WEAPONS_SWORD_FK
FOREIGN KEY (Id_Weapon)
REFERENCES Weapon (Id_Weapon)
NOT DEFERRABLE;

ALTER TABLE Staff ADD CONSTRAINT WEAPONS_STAFF_FK
FOREIGN KEY (Id_Weapon)
REFERENCES Weapon (Id_Weapon)
NOT DEFERRABLE;

ALTER TABLE Weapon_Class ADD CONSTRAINT WEAPON_WEAPON_CLASS_FK
FOREIGN KEY (Id_Weapon)
REFERENCES Weapon (Id_Weapon)
NOT DEFERRABLE;

ALTER TABLE Player_Team ADD CONSTRAINT PLAYER_PLAYER_TEAM_FK
FOREIGN KEY (Id_Player)
REFERENCES Player (Id_Player)
NOT DEFERRABLE;

ALTER TABLE Character_Build  ADD CONSTRAINT PLAYER_CHARACTER_BUILD__FK
FOREIGN KEY (Id_Player)
REFERENCES Player (Id_Player)
NOT DEFERRABLE;

ALTER TABLE Game_Team ADD CONSTRAINT PLAYER_TEAM_GAME_TEAM_FK
FOREIGN KEY (Id_PT)
REFERENCES Player_Team (Id_PT)
NOT DEFERRABLE;

ALTER TABLE Game ADD CONSTRAINT MAP_GAME_FK
FOREIGN KEY (Id_Map)
REFERENCES Map (Id_Map)
NOT DEFERRABLE;

ALTER TABLE Game_Team ADD CONSTRAINT GAME_GAME_TEAM_FK
FOREIGN KEY (Id_Game)
REFERENCES Game (Id_Game)
NOT DEFERRABLE;



