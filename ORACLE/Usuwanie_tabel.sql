drop table nationality CASCADE CONSTRAINTS;
drop table Race CASCADE CONSTRAINTS;
drop table Team CASCADE CONSTRAINTS;
drop table Spell_Kind CASCADE CONSTRAINTS;
drop table SM_SK CASCADE CONSTRAINTS;
drop table Class CASCADE CONSTRAINTS;
drop table Event_Type CASCADE CONSTRAINTS;
drop table Event CASCADE CONSTRAINTS;
drop table Game_Mode CASCADE CONSTRAINTS;
drop table Specialization CASCADE CONSTRAINTS;
drop table Spec_SM CASCADE CONSTRAINTS;
drop table Class_Spec_Rac CASCADE CONSTRAINTS;
drop table Weapon CASCADE CONSTRAINTS;
drop table Weapon_Class CASCADE CONSTRAINTS;
drop table Staff CASCADE CONSTRAINTS;
drop table Sword CASCADE CONSTRAINTS;
drop table Sword_Kind CASCADE CONSTRAINTS;
drop table Shield CASCADE CONSTRAINTS;
drop table Bow CASCADE CONSTRAINTS;
drop table Player CASCADE CONSTRAINTS;
drop table Character_Build CASCADE CONSTRAINTS;
drop table Map CASCADE CONSTRAINTS;
drop table Game CASCADE CONSTRAINTS;
drop table Game_Team CASCADE CONSTRAINTS;
drop table Player_Team CASCADE CONSTRAINTS;

 